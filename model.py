# author: "Jean-Etienne Morlighem <jemorlighem@vegetalsignals.com>"
# date: 2023-01


if __name__ == "__main__":
    from lib import common
else:
    from .lib import common


def feed_length(data):
    return len(data)


def feed_sum(data):
    return sum(data)


def feed_average(data):
    return common.list_average(data)


def feed_median(data):
    return common.list_median(data)


def feed_stdev(data):
    return common.list_stdev(data)

